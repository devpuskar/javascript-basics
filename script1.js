//Addtion,Mul,Div with pure javascript

// Declaring Variable

// var a = 10;
// var b = 20;
// var sum = a+b;

//Use let instead of varibles because of scope problem, 
// which you may face in big projects
let a = 10;
let b = 20;
let sum = a+b;
let mul = a*b;
let div = a%b; //% means reminder



//document.write Way
// document.write(sum); // printing 

//Alert Way
// alert(sum) //alert way to present data

// alert("Hello Everyone") //alert string 

//console log Way
console.log(sum)